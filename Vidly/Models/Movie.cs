﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Vidly.Models
{
    public class Movie
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Release Date")]
        public DateTime Release_Date { get; set; }
        [Required]
        [Display(Name = "Date Add")]
        public DateTime Date_Insert { get; set; }

        public Gender Gender { get; set; }

        [Required]
        [Display(Name = "Gender")]
        public int GenderId { get; set; }

        [Required]
        [Range(1, 20)]
        public int Stock { get; set; }


    }
}