namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class insert_movies_values : DbMigration
    {
        public override void Up()
        {
            Sql("Insert into Movies (Name,Release_Date,Stock,GenderId,Date_Insert) values ('Angover',CAST('3/12/1990' AS DATETIME),3,1,CAST('3/12/1990' AS DATETIME))");
            Sql("Insert into Movies (Name,Release_Date,Stock,GenderId,Date_Insert) values ('Die Hard',CAST('4/11/1991' AS DATETIME),5,2,CAST('3/12/1990' AS DATETIME))");
            Sql("Insert into Movies (Name,Release_Date,Stock,GenderId,Date_Insert) values ('The Terminator',CAST('5/10/1992' AS DATETIME),2,2,CAST('3/12/1990' AS DATETIME))");
            Sql("Insert into Movies (Name,Release_Date,Stock,GenderId,Date_Insert) values ('Toy Atory',CAST('6/09/1993' AS DATETIME),6,3,CAST('3/12/1990' AS DATETIME))");
            Sql("Insert into Movies (Name,Release_Date,Stock,GenderId,Date_Insert) values ('Titanic',CAST('7/08/1994' AS DATETIME),6,4,CAST('3/12/1990' AS DATETIME))");

        }

        public override void Down()
        {
        }
    }
}
