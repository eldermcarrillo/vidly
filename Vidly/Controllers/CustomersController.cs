﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    public class CustomersController : Controller
    {
        private ApplicationDbContext _context;
        public CustomersController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        //public ViewResult Index()
        //{
        //    var customers = _context.Customers.ToList();
        //    return View(customers);
        //}
        [Route(@"Customers/Customer/")]
        public ViewResult Customer()
        {
            //var customer = _context.Customers.Include(c => c.MembershipType).ToList();
            return View();
        }

        //Detail customers
        //[Route(@"Customers/Details/{Id}")]
        //public ActionResult Details(int Id)
        //{
        //    var customer = _context.Customers.Include(c => c.MembershipType).SingleOrDefault(c => c.Id == Id);
        //    if (customer == null)
        //        return HttpNotFound();

        //    return View(customer);
        //}

        [Route(@"Customers/New/")]
        public ActionResult New()
        {
            var membershiptypes = _context.MembershipType.ToList();
            var viewmodel = new CustomerFormViewModel
            {
                MembershipType = membershiptypes,
                Customer = new Customer()
            };
            return View("CustomerForm", viewmodel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save_Update(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                var ViewModel = new CustomerFormViewModel
                {
                    Customer = customer,
                    MembershipType = _context.MembershipType.ToList()
                };
                return View("CustomerForm", ViewModel);

            }
            if (customer.Id == 0)
            {
                _context.Customers.Add(customer);
            }
            else
            {
                var customerInDB = _context.Customers.SingleOrDefault(c => c.Id == customer.Id);
                customerInDB.Name = customer.Name;
                customerInDB.IsSubscribedToNewsletter = customer.IsSubscribedToNewsletter;
                customerInDB.MembershipTypeId = customer.MembershipTypeId;
                customerInDB.Birthdate = customer.Birthdate;
            }
            _context.SaveChanges();
            return RedirectToAction("Customer", "Customers");
        }
        public ActionResult Edit(int Id)
        {
            var customer = _context.Customers.SingleOrDefault(c => c.Id == Id);
            if (customer == null)
                return HttpNotFound();

            var viewmodel = new CustomerFormViewModel
            {
                MembershipType = _context.MembershipType.ToList(),
                Customer = customer
            };
            return View("CustomerForm", viewmodel);
        }
    }
}