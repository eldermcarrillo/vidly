﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vidly.Models
{
    public class validation_form_movie : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var movie = (Movie)validationContext.ObjectInstance;

            if (movie.Stock <= 20 && movie.Stock >= 1)
                return ValidationResult.Success;

            if (movie.Stock < 1 || movie.Stock > 20)
                return new ValidationResult("The field number in stock must be  between 1 and 20.");

            return ValidationResult.Success;
        }
    }
}