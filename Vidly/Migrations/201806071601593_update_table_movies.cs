namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_table_movies : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Movies", "Date_Insert", c => c.DateTime(nullable: false));
            DropColumn("dbo.Movies", "Create");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Movies", "Create", c => c.DateTime(nullable: false));
            DropColumn("dbo.Movies", "Date_Insert");
        }
    }
}
