﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;
using System.Data.Entity;

namespace Vidly.Controllers
{
    public class MoviesController : Controller
    {
        // GET: Movies
        private ApplicationDbContext _context;
        public MoviesController()
        {
            _context = new ApplicationDbContext();
        }
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [Route(@"Movies/Movie/")]
        public ActionResult Movie()
        {
            //var movies = _context.Movies.Include(c => c.Gender).ToList();
            return View();
        }

        //[Route(@"movies/released/{year}/{month:maxlength(2):range(1,12)}")]
        //public ActionResult ByreleaseDate(int year, int month)
        //{
        //    return Content(year + "/" + month);
        //}

        [Route(@"Movies/Edit/{Id}")]
        public ActionResult Edit(int Id)
        {
            var movie = _context.Movies.Include(c => c.Gender).SingleOrDefault(c => c.Id == Id);
            var genders = _context.Gender.ToList();
            var ViewModel = new MovieFormViewModel
            {
                Movie = movie,
                Genders = genders

            };
            if (movie == null)
                return HttpNotFound();

            return View("MoviesForm", ViewModel);
        }

        [Route(@"Movies/New/")]
        public ActionResult New()
        {
            var genders = _context.Gender.ToList();
            var ViewModel = new MovieFormViewModel
            {
                Genders = genders,
                Movie = new Movie()
            };
            return View("MoviesForm", ViewModel);
        }
        [Route(@"Movies/Save_Update")]
        public ActionResult Save_Update(Movie movie)
        {
            if (!ModelState.IsValid)
            {
                var ViewModel = new MovieFormViewModel
                {
                    Movie = movie,
                    Genders = _context.Gender.ToList()
                };
                return View("MoviesForm", ViewModel);
            }
            if (movie.Id == 0)
            {
                movie.Date_Insert = DateTime.Now;
                _context.Movies.Add(movie);
            }
            else
            {
                var MovieinDb = _context.Movies.SingleOrDefault(c => c.Id == movie.Id);

                MovieinDb.Name = movie.Name;
                MovieinDb.Release_Date = movie.Release_Date;
                MovieinDb.Stock = movie.Stock;
                MovieinDb.GenderId = movie.GenderId;
                MovieinDb.Date_Insert = MovieinDb.Date_Insert;
            }
            _context.SaveChanges();
            return RedirectToAction("Movie", "Movies");
        }
    }
}