﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vidly.Models;

namespace Vidly.ViewModels
{
    public class MovieFormViewModel
    {
        public Movie Movie { get; set; }
        public Customer Customer { get; set; }
        public List<Customer> Customers { get; set; }

        public Gender Gender { get; set; }
        public IEnumerable<Gender> Genders { get; set; }
        //public MembershipType MembershipTypes { get; set; }
    }
}