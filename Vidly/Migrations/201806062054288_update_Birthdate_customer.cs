namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class update_Birthdate_customer : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE Customers SET Birthdate = CAST('3/12/1990' AS DATETIME) WHERE Id = 2");
        }

        public override void Down()
        {
        }
    }
}
